# Pivô

Pivô é uma palavra que, como nossa plataforma quer ser, é empregada em diversas áreas. Enquanto substantivo, na botânica: a raiz principal de uma árvore; na mecânica: o eixo vertical fixo a uma peça e encaixado no oco de outra peça que, assim, adquire o movimento giratório; na economia, é uma tática de negócios importante que pode definir se o projeto irá morrer ou crescer. Com um nome já de tantos significados, a responsabilidade não é pequena, nem nossas ambições.

Nossa plataforma é o eixo de ideias, pessoas e projetos conectando-os e dando-lhes movimento.

Já é obrigatório,segundo a lei 13.005/2014, 10% da carga horária de todo aluno universitario deve ser destinado a extensão. A nossa plataforma se justifica na obrigatoriedade da extensão e na demanda constante de soluções para os problemas das cidades.  

## Funcionalidades da plataforma

* Expor ideias ou demandas (lapidadas ou não), como quem descreve um problema ou inicia um projeto.

* Ao fazer isso, na descrição da postagem devem ser definidas palavras-chave, dessa forma facilitando buscas e associações de demandas e pessoas com prováveis soluções empregáveis.

* Exposição de projetos já em execução e associação com pessoas e demandas. Essa proposta evitaria a redundância de projetos, uma chance de confrontar projetos parecidos e estimular sua parceria quando viável.
